package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.service.IProjectService;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.AbstractException;
import ru.tsc.chertkova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.model.Project;

import java.util.Date;

public class ProjectService extends AbstractUserOwnerService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String description) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String description, @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final Project project = create(userId, name, description);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(@Nullable final String userId, @Nullable final String id,
                              @Nullable final String name, @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id,
                                           @Nullable final Status status) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

}
