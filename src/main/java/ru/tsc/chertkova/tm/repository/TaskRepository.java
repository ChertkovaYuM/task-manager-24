package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @Nullable
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return models.stream()
                .filter(item -> item.getProjectId() != null
                        && item.getProjectId().equals(projectId)
                        && item.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

}
